---
layout: 2020/post
section: proposals
category: talks
title: Linux, the gothic cathedral of our times
---

[This talk will be delivered (likely) in a mix of English and Spanish]

[Esta charla se realizará (muy posiblemente) en una mezcla de español e inglés]

Eric Raymond claimed that open source is developed in a "Bazaar" style
software development process, while proprietary software is developed more in a "Cathedral" style. In this presentation I would argue that the great software cathedrals of our time are being developed in the open, just like the great cathedrals of our world. In particular, I would describe how Linux has not only been able to build one of the great software systems of our time, but how it has changed software engineering in general.

## Formato de la propuesta

Charla plenaria

## Ponente

Daniel German, University of Victoria

Daniel is professor in the department of computer science at the University of Victoria. His main area of research is software engineering. In particular, software evolution, open source and intellectual property. In his spare time he does research in computational photography. He also enjoys programming. Recently, he has co-authored [The 2020 Linux Kernel History Report](https://www.linuxfoundation.org/blog/2020/08/download-the-2020-linux-kernel-history-report/), which he will present in part in this talk.
