---
layout: 2021/post
section: proposals
category: projects
title: Wendigo
---

Wendigo es una herramienta de automatización de navegadores orientada a testing (end-to-end y front-end) construida encima de Puppeteer.

-   Web del proyecto: <https://github.com/angrykoala/wendigo>

### Contacto(s)

-   Nombre: angrykoala
-   Email: angrykoala@outlook.es
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/angrykoala>

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información del proyecto.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del proyecto.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.

