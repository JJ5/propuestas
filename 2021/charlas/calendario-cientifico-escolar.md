---
layout: 2021/post
section: proposals
category: talks
author: Pablo Gutiérrez Toral
title: Calendario Científico Escolar. Hacia un proyecto de divulgación científica público más colaborativo y accesible 
---

La publicación de un calendario con aniversarios científicos y tecnológicos diarios surge en 2011 en Rumanía y su éxito hace que otros países reproduzcan la idea. En 2019, un grupo de personas voluntarias decidimos impulsarla también en España, adaptándola para intentar lograr un proyecto público de cultura libre, más participativo y accesible.
En esta charla corta resumiremos los desafíos y logros de estos años y nuestras perspectivas para seguir mejorando en el futuro. 

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

El Calendario Científico Escolar 2021 es una iniciativa que recoge un aniversario científico o tecnológico para cada día del año con el objetivo de fomentar la cultura científica en los centros educativos y poner a disposición del alumnado modelos referentes que promuevan las vocaciones STEM. 

El proyecto está coordinado desde el Instituto de Ganadería de Montaña (IGM), un centro mixto del CSIC y la Universidad de León y dispone de financiación de la Fundación Española de Ciencia y Tecnología (FECYT). Para su elaboración, contamos con la colaboración de numerosas entidades y personas voluntarias del mundo científico y educativo, quienes trabajan con nuestro equipo en el desarrollo del mismo.

El calendario se acompaña de una guía didáctica con orientaciones para su aprovechamiento educativo transversal en las clases. Estas propuestas didácticas parten de los principios de inclusión, normalización y equidad y se acompañan de pautas generales de accesibilidad.

El calendario, la guía y el resto de materiales generados están registrados bajo la licencia Creative Commons 4.0 BY y alojados en el repositorio institucional Digital.CSIC y la plataforma de desarrollo colaborativo GitHub, pudiendo descargarse a través en la página web del IGM (http://www.igm.ule-csic.es/calendario-cientifico). Además, para favorecer su uso en las aulas y alcanzar nuevos públicos, todo el material está disponible en varios idiomas. 

Actualmente también disponemos de cuentas de Twitter y Telegram que publican las efemérides diarias, pero nos gustaría tener presencia en RRSS libres. 

En relación a la temática de este congreso, nuestro trabajo busca contribuir a la alfabetización científica de la población a través de un proyecto que aspira a ser abierto, colaborativo y cada vez más accesible. Para ello, hemos utilizado formatos que estamos intentando enriquecer año a año. Esperamos poder compartir nuestra experiencia al respecto y discutir algunas de las propuestas de futuro y de los desafíos a los que nos enfrentamos a la hora de seguir creciendo y mejorando.

Esta charla se plantea en paralelo a la impartida por Jorge Aguilera, colaborador de nuestro proyecto y que versará sobre algunos aspectos más técnicos ligados al proyecto.

-   Web del proyecto: <http://www.igm.ule-csic.es/calendario-cientifico>

## Público objetivo

Esta charla se dirige a todos los públicos, aunque nuestro principal objetivo (de la misma y del proyecto) es llegar a personas que puedan estar más distanciadas de la actividad investigadora, para fomentar su acceso e interés hacia la ciencia y la tecnología.

## Ponente(s)

Pablo Toral, Ana Alonso, Belén Ballesteros, Jorge Aguilera, Gonzalo Hervás, Pilar Frutos y Sara Vicente.

Esta ponencia es resultado de nuestro trabajo en equipo. Para hacer más fácil y operativo su desarrollo en línea, la charla será impartida por Pablo Toral, investigador del IGM (CSIC-ULE), coordinador del proyecto Calendario Científico Escolar y responsable habitual de las actividades de comunicación del mismo.

### Contacto(s)

-   Nombre: Pablo Gutiérrez Toral
-   Email: pablo.toral@csic.es
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/CalCientifico>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/calendario-cientifico-escolar>

## Comentarios

El calendario también dispone de una cuenta de Telegram que publica las efemérides diarias en diferentes idiomas: https://t.me/CalendarioCientifico

Los ficheros para importar el calendario en tu propia agenda están disponibles en: https://calendario-cientifico-escolar.github.io/ical/

## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
