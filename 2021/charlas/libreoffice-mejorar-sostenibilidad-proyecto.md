---
layout: 2021/post
section: proposals
category: talks
author: Italo Vignoli
title: LibreOffice Mejorar la sostenibilidad del proyecto
---

LibreOffice se anunció en 2010. Después de 10 años, era necesario actualizar la estrategia, para mejorar el modelo de sostenibilidad. Las empresas no están apoyando el proyecto tanto como los usuarios individuales. Con el tiempo, esto puede representar una amenaza para la sostenibilidad del proyecto. Hemos cambiado nuestra estrategia para educar a las empresas sobre el enfoque correcto del software libre, devolviendo para asegurar la sostenibilidad a largo plazo del proyecto LibreOffice.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

LibreOffice se anunció en 2010 para relanzar la innovación del ya estancado proyecto OpenOffice. Tras 10 años de crecimiento, era necesario revisar y actualizar la estrategia en función de la evolución del mercado de las suites ofimáticas, para mejorar el modelo de sostenibilidad.
De hecho, las empresas -aunque despliegan LibreOffice para ahorrar dinero respecto a las soluciones propietarias- no están apoyando el proyecto tanto como los usuarios individuales. Con el tiempo, esto puede representar una amenaza para la sostenibilidad del proyecto, ya que el desarrollo puede verse frenado.
Por lo tanto, hemos cambiado nuestra estrategia para educar a las empresas sobre el enfoque correcto del software libre, devolviendo en una de las muchas formas disponibles: comprar la versión LTS del ecosistema, financiar el desarrollo de una característica específica, pagar por la solución de un error o una regresión, etc.
El enfoque correcto de las empresas aseguraría la sostenibilidad a largo plazo del proyecto LibreOffice, y la evolución del formato de archivo estándar ISO Open Document Format (ODF) para una verdadera interoperabilidad.

-   Web del proyecto: <https://www.libreoffice.org>

## Público objetivo

Defensores del código abierto, usuarios de software de productividad corporativos e individuales

## Ponente(s)

Italo Vignoli es miembro fundador de The Document Foundation, presidente emérito de la Associazione LibreItalia, miembro de la junta directiva de la Open Source Initiative (OSI) y copresidente del proyecto abierto OASIS de defensa del ODF. Codirige el marketing, las relaciones públicas y las relaciones con los medios de comunicación de LibreOffice, copreside el programa de certificación y es portavoz del proyecto.

### Contacto(s)

-   Nombre: Italo Vignoli
-   Email: italo@documentfoundation.org
-   Web personal: <https://www.vignoli.org>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/italovignoli>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
