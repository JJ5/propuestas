---
layout: 2021/post
section: proposals
category: talks
author: Fabio Durán-Verdugo
title: Handibox - Una herramienta para integrar personas con problemas motores.
---

Handibox es una herramienta de accesibilidad que permitirá la interacción humano-Computador a personas que tienen algún grado de discapacidad motora. Mediante su uso las personas tendrán la posibilidad de realizar actividades elementales como navegar por Internet, escribir correos electrónicos, enviar mensajes, etc. Adicionalmente Handibox podría servir como apoyo en actividades de fisioterapia para personas que deban realizar ejercicios que involucren el movimiento del cuello.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

La Organización Mundial de la Salud (OMS) a través de sus distintos canales de comunicación, constantemente realiza un esfuerzo para crear conciencia e informar sobre
datos relevantes acerca de las dificultades a los que se enfrentan las personas en situación de discapacidad. Algunos de sus reportes e indicadores nos desafían a quienes trabajamos en educación y en particular en el área de las tecnologías de la información y comunicación (TIC), a desarrollar y establecer mecanismos o estrategias efectivas de enseñanza-aprendizaje para incluir adecuadamente a este sector de la población. Entre ellas, podemos destacar:

* Más de 1000 millones de personas padece algún tipo de discapacidad, lo que representa cerca del 15 % de la población mundial. De este grupo, casi 200 millones tienen disminución funcional grave. De forma dramática, el envejecimiento acelerado de la población y la gran prevalencia de enfermedades crónicas, orienta que estas cifras irán en aumento.
* La discapacidad afecta de manera desproporcionada a las poblaciones vulnerables. Los países de ingresos bajos tienen una mayor prevalencia de discapacidades que los países de ingresos altos. La discapacidad es más común entre las mujeres, las personas mayores y los niños y adultos que son pobres.
* Los niños en situación de discapacidad tienen menos probabilidades de ser escolarizados que los niños sin discapacidad. Se observan desfases entre las tasas de finalización de los estudios para todos los grupos de edad y en todos los contextos, con contrastes más pronunciados en los países más pobres. Esto dramatiza aún más en época de pandemia en donde las herramientas computacionales no están pensadas en su general para el uso de personas en situación de discapacidad. Por ejemplo, la diferencia entre el porcentaje de niños y niñas en situación de discapacidad que asisten a la escuela primaria oscila entre el 10 % en la India y el 60 % en Indonesia.
* Las personas en situación de discapacidad tienen más probabilidades de estar desempleadas que las personas sin discapacidad. Los datos mundiales indican que las tasas de empleo son más bajas para los hombres (53 %) que las mujeres en situación de discapacidad (20 %).
* Las personas en situación de discapacidad pueden vivir y participar en la comunidad, sin embargo incluso en los países de ingresos altos, entre el 20 % y el 40 % de esta población  o ven por lo general satisfechas sus necesidades de forma independiente y requieren de asistencia, que por lo general es entregada por amigos o familiares.

Handibox es una herramienta pensada en accesibilidad que intenta ser paso más para una integración real, permitiendo además aunar otras herramientas de accesibilidad ya existentes en GNOME y en lo posible entregar más experiencias que nos permitan mejorar las características para las aplicaciones se puedan adaptar a un mayor número de usuarios con problemas de discapacidad e idealmente construir y promover nuevas aplicaciones entorno a Handibox, por ejemplo una aplicación de reconocimiento de voz.

Actualmente Handibox es desarrollado por  un grupo de académicos y estudiantes que pertenece a la carrera de pregrado de Ingeniería Civil en Bioinformática de la Universidad de Talca en Chile, que usa desde el primer año académico herramientas de software libre y a quienes se les presenta como una alternativa real la filosofía del FLOSS. 

Creemos que si integramos con herramientas prácticas a personas con variados problemas de discapacidad que le impidan el uso del ordenador de manera normal, podremos extraer experiencias para mejorar la aplicación y el entorno GNOME, en especial a lo que en accesibilidad corresponde.

La realidad nos ha dicho que atraer a personas es difícil y que también que es complejo desarrollar herramientas de software para accesibilidad, esto lo podemos demostrar con la herramienta mouse-trap que es la herramienta en la cual se inspiró Handibox pero que lamentablemente está descontinuada y en distribuciones actuales presenta diversos problemas en su uso. Sumado a lo anterior, también nos respaldamos en la poca diversidad de herramientas de accesibilidad existentes.

Handibox está desarrollado en Python con Gtk3 y opencv y se encuentra en desarrollo en el siguiente repositorio: https://gitlab.gnome.org/fabioduran/handibox


-   Web del proyecto: <https://gitlab.gnome.org/fabioduran/handibox>

## Público objetivo

Publico en general

## Ponente(s)

Fabio Durán-Verdugo
Alejandro Valdés-Jiménez
Matías Rojas-Tapia

### Contacto(s)

-   Nombre: Fabio Durán-Verdugo
-   Email: fabioduran@gnome.org
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/fabioduran>
-   GitLab: <https://gitlab.com/fabioduran>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
