---
layout: 2021/post
section: proposals
category: talks
author: Antonio Montes de Miguel
title: Creación Recursos Espaciales Aprendizaje XR con FLoss
---

Charla sobre la Creación de Recursos Espaciales de Realidad eXtendida con FLOSS, enfocado desde y hacia el Medio Ambiente Rural , con-ciencia Eco-tecnológica.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Buenas, mi nombre es Antonio Montes, colaboro con el Grupo Avfloss de MediaLabPrado y propongo una charla sobre el tema que estoy investigando en el doctorado de Bellas Artes cuyo título inicial de tesis fue: "CREA con SAL en el MAR".

Creación de
Recursos
Espaciales de
Aprendizaje

con

Software
Abierto y
Libre

en
el

Medio
Ambiente
Rural.

Los Principales temas enlazados son:

- La importancia del ESPACIO.  Tanto el entorno físico de Enseñanza~Aprendizaje, (centrándome en el Natural/Rural). Como de la relación de los contenidos con la distribución en el espacio (método "Loci"/mapas mentales) en este caso en el espacio XR 3D extendido.
- Construcción/compartición del conocimiento de modo colaborativo. Espacios Virtuales de Colaboración.
- POLILLA o LUCIÉRNAGA; Es decir, ser esclav@ deslumbrado por la tecnología, estampándose "adictivamente" contra la pantalla de colores. O ser Creadora consiente de los procesos y conocedora de la misma.
- Gestión del gasto/consumo ENERGÉTICO de estos procesos.
- El uso, capacidades y bondades del FLOSS para éstos medios/fines.
- Creación y trazados del gesto en el Espacio 3D y  en entornos XR con FLOSS.
- El ejemplo de Blender + SPOKE para Crear entornos virtuales para Mozilla HUBS. y su probable integración con Plataformas libres como MOODLE.
- Propuesta de Colaboraciones.


-   Web del proyecto: <http://8d2.es/course/view.php?id=41>

## Público objetivo

- Creador@s 3D.
- Profesor@s.
- Programadores con ganas de colaborar en integración espacios virtuales + Moodle.
- Agentes en entornos Rurales ( Escuelas, Asociaciones,...)

## Ponente(s)

Antonio Montes de Miguel

### Contacto(s)

-   Nombre: Antonio Montes de Miguel
-   Email:
-   Web personal: <http://inventadero.com/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/inventadero>
-   GitLab: <https://gitlab.com/inventadero>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/inventadero>

## Comentarios

El formato de la charla sería jitsy y en paralelo un espacio HUBS de Mozilla enlazado a un MOODLE en los que poder intervenir y colaborar, similar a éste → http://8d2.es/course/view.php?id=63

## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
