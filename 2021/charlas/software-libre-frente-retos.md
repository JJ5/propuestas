---
layout: 2021/post
section: proposals
category: talks
author:  Rubén Gómez Antolí
title: El software libre frente a sus retos&#58 una visión desde la experiencia
---

El software libre con Gnu/Linux al frente parece avanzar imparable hacia la conquista de todos los nichos de sistemas con, quizás, la salvedad de la ansiada dominación del escritorio.

## Formato de la propuesta

- Charla plenaria

## Descripción

El software libre con Gnu/Linux al frente parece avanzar imparable hacia la conquista de todos los nichos de sistemas con, quizás, la salvedad de la ansiada dominación del escritorio.

Las enormes labores de promoción que algunos proyectos como KDE y Gnome, entre otros, están realizando poniendo recursos para ello están dando sus frutos y, sin embargo, todavía hay espacios donde no se consigue implantar y donde hablar de software libre es algo casi utópico.

Desde la experiencia como profesional técnico, como miembro de equipos de promoción y como formador en tecnología, intentaré dar una visión global sobre los retos que todavía necesita afrontar el software libre.

-   Web del proyecto:

## Público objetivo


## Ponente(s)

Me llamo Rubén Gómez y soy inquieto, soy curioso, soy crítico y estoy comprometido; comprometido con la ciencia, con el pensamiento escéptico, con la toma de decisiones basadas en hechos contrastados; comprometido con la mejora de la sociedad, del medio ambiente, del futuro para todos los ciudadanos del orbe. Por eso hago ingeniería desde un punto de vista especial, utilizando -en la medida de lo posible- herramientas libres.

### Contacto(s)

-   Nombre: Rubén Gómez Antolí
-   Email:
-   Web personal: <https://www.mucharuina.com/>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Gitlab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [ ]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
