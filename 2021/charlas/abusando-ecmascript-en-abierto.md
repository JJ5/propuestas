---
layout: 2021/post
section: proposals
category: talks
author: tripu
title: Abusando de ECMAScript en abierto
---

JavaScript. Está en todas partes. Como lengua, es bastante abierta y sociable. Las apps web son legibles y editables, por definición (o deberían). Muchos programadores buenos (y todos los malos) usan JS, así que hay en internet un quintal de proyectos, experimentos y divertimentos con/de/contra/para JS. ¿Cuáles son las aplicaciones más perversas, los casos de uso más nicho, y las implementaciones más extravagantes de JS? Y, ¿qué se puede aprender de los trastornos y las obsesiones ajenas?

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Nos podemos reír de la fragmentación y las incompatibilidades en el mundo JavaScript todo el día (y con razón), pero en esencia JS es ECMAScript, y ES es un puñado de estándares libres y abiertos. Principalmente ECMA-262. Desde 2015, con ES6, JS se ha puesto al día y evoluciona (otros dirán involuciona) a velocidad de crucero. La ubicuidad de JS, no solo en la web sino *en todas partes* y el hecho de que es una plataforma fácil y permisiva para neófitos han hecho que florezca una plétora de flores ocurrentes y sorprendentes a su alrededor (si bien la mayoría están marchitas o en estado de putrefacción).

El personal incrusta intérpretes minimales de JS en cualquier sitio. Otros transpilan JS desde, o hacia, lenguas esotéricas (a veces en cadena). Los hay que se dedican a optimizar (o a pervertir) los recodos más oscuros del motor de JS. Otras parafilias consisten en tunear el estándar para mejorar aspectos como latencia o seguridad. Sin olvidarnos del onanismo que es tan propio de los programadores: implementaciones de JS escritas *en* JS, emulación de JS dentro del motor JS del navegador, introspección, etc.

Veremos algunos de estos casos de uso esotéricos, nos sorprenderemos de que sean posibles, nos compadeceremos de las pobres almas aburridas que los concibieron y los mantienen, e intentaremos aprender lo que se pueda de ellos.

-   Web del proyecto:

## Público objetivo

Programadores en general, y programadores de JS en particular.

## Ponente(s)

tripu, ingeniero informático por la UGR, con > 15 años de experiencia en distintas empresas y capas/áreas de tecnología, y programador que disfruta odiando JS en todas sus variantes desde hace más de una década. He dado charlas técnicas en varios eventos, y he estado metido en sectas de software libre desde mis balbuceos universitarios. Puedo caminar con las manos durante más de cinco segundos; diez cuando no hay viento y he desayunado fuerte.

### Contacto(s)

-   Nombre: tripu
-   Email: t@tripu.info
-   Web personal: <https://tripu.info/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@tripu>
-   Twitter: <https://twitter.com/tripu>
-   GitLab: <https://gitlab.com/tripu.info>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/tripu>

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
