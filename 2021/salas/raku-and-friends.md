---
layout: 2021/post
section: proposals
category: devrooms
community: Raku & Perl mongers
title: Raku & friends
---

## Descripción de la sala

[Raku](https://raku.org) is a multi-paradigm language _created for the
next 100 years_. Its wealth of features and expressivity make it an
ideal choice for creative programming. We consider friends most other
languages, or all of them, really, but if you need to ask, we would be
interested in technologies or services that could be interfaced with
Raku (data stores, APIs), or programming techniques (grammars, regular
expressions, functional programming, concurrent programming) that
could be of interest for the Raku community, even if code samples are
not written in the language. In a word, we want to attract any
programmer that enjoys their job, and wants to inspire and be inspired
by other people like them.

In this edition, this devroom is going to be composed of several short
keynotes, that were invited to propose a talk. 


## Talks and speakers

The devroom will start with a short intro by JJ Merelo.

### Cecilia Merelo, *Vue + Raku : a new facade* (40 minutes)

Raku is usually used as backend language so let's give a frontend to
our Raku scripts!

**Cecilia Merelo** (she/her) is junior software engineer in Badger
Maps.

### Joelle Maslak, *Designing a 'In-Meeting' Indicator with Raku* (20 minutes)

Joelle talks about the design decisions behind her "busy indicator"
light, which lets others know if she is in a meeting or if she can be
interrupted. Her busy indicator script will be discussed, showing how
Raku is an exceptional glue language, and can tie together calls to
external executables, operating system status information, network
access, and USB device interaction easily. I expect this talk to be
interesting for all skill levels, including beginners.

**Joelle Maslak** (she/her) is Network Engineer @ Netflix (Open Connect)

### Paula de la Hoz, *Malware analysis scripting with Raku* (20 minutes)

When it comes to study malware and cyber threats, some cases aren't as
clear as others an require further analysis of the samples. For this
purpose, some interesting and well known tools such as Yara, Cutter,
Intezer and others are widely used, but for the sake of self-made I
will introduce a brief Raku scripting for malware analysis using
rules. This will help iterate a binary using the mere Terminal looking
for malicious cyber-genes.

**Paula de la Hoz** (she/her) is Threat Hunting analyst in Telefonica
Tech, [Interferencias](https://interferencias.tech) co-founder.

### Liz Mattijsen, *Sigils.  Once you know them, you'll love them in the Raku Programming Language.*, (20 minutes)

The roots of the Raku Programming Language are in Perl.  The use of
sigils in Perl is seen by many as the reason it is hard to read any
code written in Perl.  In this presentation, I will show how sigils
can be used in the Raku Programming Language, and how they actually
*help* in making code understandable.  And should you decide you don't
like sigils after all, how you can make Raku programs without them.

**Elizabeth Mattijsen** (she/her) is Raku Programming Language core developer,
Member of the Raku Steering Council and  Former recipient of the White
Camel Award.

### Clifton Wood, *A Pattern for Creating NativeCall Bindings* (30 minutes)

Or, how he wrote around 560000 lines of Raku in 3 years.

**Clifton Wood** is a frequent Raku contributor.

## Target

English-speaking coding enthusiast, with or without any prior experience with the language.


